using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using TinyTest;

public class UnitTestWindow : EditorWindow
{
    private enum TestsVisible
    {
        All,
        Failed,
        Passed,
        Ignored
    }

    // Sizes
    private const int CIRCLE_ICON_RADIUS = 9;
    private const int FILTER_BUTTONS_MAX_WIDTH = 80;
    private const int IGNORE_BUTTON_MAX_WIDTH = 80;
    private const int AUTO_RUN_BUTTON_MAX_WIDTH = 65;
    private const int RUN_BUTTON_MAX_WIDTH = 35;

    // Save keys
    private static readonly string AUTO_RUN_TESTS_KEY = PlayerSettings.productGUID + "_autoRunTests";
    private static readonly string IGNORED_TESTS_KEY = PlayerSettings.productGUID + "_ignoredUnitTests";
    private const string DID_DISMISS_DETAIL_TIP_KEY = "DidDissmissUnitTestDetailsTip"; 

    // Colors
    private static readonly Color FAIL_COLOR = new Color(1f, 0.4f, 0.4f);
    private static readonly Color FAIL_SELECTED_COLOR = new Color(1f, 0, 0);
    private static readonly Color PASS_COLOR = new Color(0.5f, 1, 0.5f);
    private static readonly Color PASS_SELECTED_COLOR = new Color(0, 1, 0);
    private static readonly Color IGNORE_COLOR = new Color(1, 1, 0.5f);
    private static readonly Color IGNORE_SELECTED_COLOR = new Color(1, 1, 0);
    
    private static UnitTestWindow _instance;

    private static bool _shouldRunTests;

    private TestItem _selectedTestItem; 
    [SerializeField] List<TestItem> _testItems;
    private HashSet<string> _ignoredTestItems;
    private Texture2D _circleIcon;
    private Vector2 _detailsScrollPos;
    private Vector2 _testsScrollPos;
    private bool _autoRunTests;

    private TestsVisible _testsVisible;

    [MenuItem ("Window/Tiny Test/Unit Test Window")]
    private static void Init()
    {
        // Get existing open window or if none, make a new one:
        _instance = GetWindow(typeof(UnitTestWindow)) as UnitTestWindow;
        _instance.title = "Unit tests";
    }

    [MenuItem("Window/Tiny Test/Send Feedback")]
    private static void SendFeedback()
    {
        Application.OpenURL(@"mailto:mathiassoeholm@gmail.com");
    }

    static UnitTestWindow()
    {
        _shouldRunTests = true;
        EditorApplication.update += Update;
    }

    private void OnEnable()
    {
        _shouldRunTests = _autoRunTests = EditorPrefs.GetBool(AUTO_RUN_TESTS_KEY, true);
        _selectedTestItem = null;

        LoadIgnoredTests();

        if (_autoRunTests)
        {
            RunTests();
        }
    }

    private void OnDestroy()
    {
        EditorApplication.update -= Update;
    }

    private static void Update()
    {
        if (_instance == null)
        {
            _instance = GetWindow(typeof(UnitTestWindow)) as UnitTestWindow;
        }
        
        if (_instance._autoRunTests && _shouldRunTests && _instance != null)
        {
            _instance.RunTests();
            
        }
    }

    private void RemoveUnexistingIgnoredTests()
    {
        for (int i = _ignoredTestItems.Count - 1; i >= 0; i--)
        {
            if (_testItems.All(t => t.Name != _ignoredTestItems.ElementAt(i)))
            {
                _ignoredTestItems.Remove(_ignoredTestItems.ElementAt(i));
            }
        }

        SaveCurrentIgnoreList();
    }

    private void LoadIgnoredTests()
    {
        // Get ignored tests
        if (EditorPrefs.HasKey(IGNORED_TESTS_KEY))
        {
            string ignoredTests = EditorPrefs.GetString(IGNORED_TESTS_KEY);

            if (String.IsNullOrEmpty(ignoredTests))
            {
                _ignoredTestItems = new HashSet<string>();
            }
            else
            {
                _ignoredTestItems = new HashSet<string>(ignoredTests.Split('|'));
            }
        }
        else
        {
            _ignoredTestItems = new HashSet<string>();
        }
    }

    private void RunTests()
    {
        _selectedTestItem = null;
        _testItems = UnitTestManager.RunTests(_ignoredTestItems);

        _shouldRunTests = false;
        RemoveUnexistingIgnoredTests();
    }

    private void IgnoreTest(TestItem test)
    {
        if (_ignoredTestItems.Contains(test.Name))
        {
            // Unignore
            _ignoredTestItems.Remove(test.Name);

            // Run test
            var newTestItem = UnitTestManager.RunTestWithName(test.Name);

            // Remove old test
            _testItems.Remove(test);

            // Add new if exists..
            if (newTestItem != null)
            {
                _testItems.Add(newTestItem);
            }

            _selectedTestItem = newTestItem;
        }
        else
        {
            // Ignore
            _ignoredTestItems.Add(test.Name);
        }

        SaveCurrentIgnoreList();
    }

    private void SaveCurrentIgnoreList()
    {
        var sb = new StringBuilder();
        for (int i = 0; i < _ignoredTestItems.Count; i++)
        {
            sb.Append(_ignoredTestItems.ElementAt(i));

            if (i != _ignoredTestItems.Count - 1)
            {
                sb.Append("|");
            }
        }

        EditorPrefs.SetString(IGNORED_TESTS_KEY, sb.ToString());
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Run", GUILayout.MaxWidth(RUN_BUTTON_MAX_WIDTH)))
        {
            RunTests();
        }

        bool autoRun = _autoRunTests;
        _autoRunTests = GUILayout.Toggle(_autoRunTests, "Auto Run", "button", GUILayout.MaxWidth(AUTO_RUN_BUTTON_MAX_WIDTH));
        if (_autoRunTests != autoRun)
        {
            EditorPrefs.SetBool(AUTO_RUN_TESTS_KEY, _autoRunTests);
        }

        bool isSelectedIgnored = _selectedTestItem != null && _ignoredTestItems.Contains(_selectedTestItem.Name);
        if (GUILayout.Button(isSelectedIgnored ? "Unignore" : "Ignore", GUILayout.MaxWidth(IGNORE_BUTTON_MAX_WIDTH)) && _selectedTestItem != null)
        {
            if (_selectedTestItem != null)
            {
                IgnoreTest(_selectedTestItem);
            }
        }

        EditorGUILayout.EndHorizontal();

        if (_testItems == null)
        {
            return;
        }

        int passedAmount = _testItems.Count(t => t.Success && !_ignoredTestItems.Contains(t.Name));
        int failedAmount = _testItems.Count(t => !t.Success && !_ignoredTestItems.Contains(t.Name));

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Toggle(_testsVisible == TestsVisible.All, "All (" + (_testItems.Count) + ")", "button", GUILayout.MaxWidth(FILTER_BUTTONS_MAX_WIDTH)))
        {
            _testsVisible = TestsVisible.All;
        }

        GUI.color = PASS_COLOR;
        if (GUILayout.Toggle(_testsVisible == TestsVisible.Passed, "Passed (" + passedAmount + ")", "button", GUILayout.MaxWidth(FILTER_BUTTONS_MAX_WIDTH)))
        {
            _testsVisible = TestsVisible.Passed;
        }

        GUI.color = FAIL_COLOR;
        if (GUILayout.Toggle(_testsVisible == TestsVisible.Failed, "Failed (" + failedAmount + ")", "button", GUILayout.MaxWidth(FILTER_BUTTONS_MAX_WIDTH)))
        {
            _testsVisible = TestsVisible.Failed;
        }
        GUI.color = Color.white;

        GUI.color = IGNORE_COLOR;
        if (GUILayout.Toggle(_testsVisible == TestsVisible.Ignored, "Ignored (" + (_ignoredTestItems.Count) + ")", "button", GUILayout.MaxWidth(FILTER_BUTTONS_MAX_WIDTH)))
        {
            _testsVisible = TestsVisible.Ignored;
        }
        GUI.color = Color.white;

        EditorGUILayout.EndHorizontal();

        _testsScrollPos = GUILayout.BeginScrollView(_testsScrollPos);

        if (failedAmount > 0 && _testsVisible != TestsVisible.Passed && _testsVisible != TestsVisible.Ignored)
        {
            if (!EditorPrefs.GetBool(DID_DISMISS_DETAIL_TIP_KEY))
            {
                EditorGUILayout.BeginHorizontal();

                if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.MaxWidth(19)))
                {
                    EditorPrefs.SetBool(DID_DISMISS_DETAIL_TIP_KEY, true);
                }
                EditorGUILayout.LabelField("Tip: Click red circles for details");

                EditorGUILayout.EndHorizontal();
            }
        }

        foreach (TestItem testItem in _testItems)
        {
            bool isIgnored = _ignoredTestItems.Contains(testItem.Name);
            
            if (_testsVisible == TestsVisible.Failed && (testItem.Success || isIgnored))
            {
                continue;
            }

            if (_testsVisible == TestsVisible.Passed && (!testItem.Success || isIgnored))
            {
                continue;
            }

            if (_testsVisible == TestsVisible.Ignored && !isIgnored)
            {
                continue;
            }

            EditorGUILayout.BeginHorizontal();

            GUILayout.Space(5);

            GUI.color = isIgnored ? testItem == _selectedTestItem ? IGNORE_SELECTED_COLOR : IGNORE_COLOR :
                testItem.Success ? testItem == _selectedTestItem ? PASS_SELECTED_COLOR : PASS_COLOR :
                testItem == _selectedTestItem ? FAIL_SELECTED_COLOR : FAIL_COLOR;

            if (_circleIcon == null)
            {
                CreateCircleIcon();
            }

            if (GUILayout.Button(_circleIcon, GUIStyle.none, GUILayout.Width(CIRCLE_ICON_RADIUS*2)))
            {
                _selectedTestItem = _selectedTestItem != testItem ? testItem : null;
            }

            GUI.color = Color.white;

            if (_selectedTestItem != testItem)
            {
                EditorGUILayout.LabelField(testItem.Name);
            }
            else
            {
                EditorGUILayout.LabelField(testItem.Name, EditorStyles.boldLabel);
            }

            EditorGUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();

        if (_selectedTestItem != null && !_selectedTestItem.Success && _selectedTestItem.Message != null)
        {
            GUILayout.Label(""); // Spacing
            GUILayout.Label("Details:");

            _detailsScrollPos = GUILayout.BeginScrollView(_detailsScrollPos, GUILayout.MaxHeight(200));

            EditorStyles.textField.wordWrap = true;
            EditorGUILayout.TextArea(_selectedTestItem.Message, GUILayout.MaxHeight(60));

            GUILayout.EndScrollView();
        }
    }

    private void CreateCircleIcon()
    {
        _circleIcon = new Texture2D(CIRCLE_ICON_RADIUS * 2, CIRCLE_ICON_RADIUS * 2, TextureFormat.RGBA32, false);

        var transparentColors = new Color[(CIRCLE_ICON_RADIUS * 2) * (CIRCLE_ICON_RADIUS * 2)];

        // Clear all pixels
        for (int i = 0; i < transparentColors.Length; i++)
        {
            transparentColors[i] = Color.clear;
        }

        _circleIcon.SetPixels(transparentColors);

        for (int x = -CIRCLE_ICON_RADIUS; x < CIRCLE_ICON_RADIUS; x++)
        {
            int height = (int)Math.Sqrt(CIRCLE_ICON_RADIUS * CIRCLE_ICON_RADIUS - x * x);

            for (int y = -height; y < height; y++)
                _circleIcon.SetPixel(x + CIRCLE_ICON_RADIUS, y + CIRCLE_ICON_RADIUS, Color.white);
        }

        _circleIcon.Apply();
    }
}
