﻿using System;
using TinyTest;

[TestFixture]
public class ExampleUnitTests
{
    private int _four;

    [SetUp]
    public void Tests_Setup()
    {
        // This runs once before each test
        _four = 4;
    }

    [Test]
    public void Is_Field_Four_Actually_Four()
    {
        Assert.AreEqual(_four, 4);
    }

    [Test]
    public void One_Plus_One_Is_Two()
    {
        Assert.AreEqual(1 + 1, 2);
    }

    [Test]
    public void New_Object_Is_Not_Null()
    {
        // Extension method to check for null.. Fancy huh?
        (new object()).IsNotNull();
    }

    [Test]
    public void Null_Is_Not_Null()
    {
        var fail = new object();
        (fail as string).IsNotNull();
    }

    [Test]
    public void One_Plus_One_Is_Three()
    {
        Assert.AreEqual(1 + 1, 3);
    }

    // Test many different cases with one method!
    [Test]
    [TestCase(5, 5, 25)]
    [TestCase(1, 5, 5)]
    [TestCase(100, 0, 0)]
    [TestCase(10, 2, 20)]
    [TestCase(-1, 7, -7)]
    public void Multiplication_Tests(int x, int y, int result)
    {
        Assert.AreEqual(x * y, result);
    }

    [TearDown]
    public void Tear_Down_Tests()
    {
        // This runs once after each test
    }
}
