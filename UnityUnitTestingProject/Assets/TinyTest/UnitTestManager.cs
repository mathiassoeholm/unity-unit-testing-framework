using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TinyTest
{
    [Serializable]
    public class TestItem
    {
        public string Name;
        public string Message;
        public bool Success;
    }

    public static class UnitTestManager
    {
        public static bool IsRunning { get; set; }

        public static List<TestItem> GetTests()
        {
            if (IsRunning)
            {
                throw new InvalidOperationException("Can't get tests while running");
            }

            return (from type in Assembly.GetExecutingAssembly().GetTypes()
                    where type.IsDefined(typeof(TestFixtureAttribute), false)
                    let methods = type.GetMethods()
                    from testMethod in methods
                    where testMethod.IsDefined(typeof(TestAttribute), false)
                    select type.Name + "::" + testMethod.Name into name
                    select new TestItem { Name = name }).ToList();
        }

        public static TestItem RunTestWithName(string testName)
        {
            if (IsRunning)
            {
                throw new InvalidOperationException("Already running tests!");
            }

            IsRunning = true;

            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (type.IsDefined(typeof(TestFixtureAttribute), false))
                {
                    MethodInfo[] methods = type.GetMethods();
                    MethodInfo testMethod = methods.FirstOrDefault(m => (m.DeclaringType + "::" + m.Name) == testName);

                    if (testMethod != null)
                    {
                        object fixtureInstance = Activator.CreateInstance(type);
                        MethodInfo setUpMethod = methods.FirstOrDefault(x => x.IsDefined(typeof(SetUpAttribute), false));
                        MethodInfo tearDownMethod = methods.FirstOrDefault(x => x.IsDefined(typeof(TearDownAttribute), false));

                        var testItem = RunTest(testMethod, setUpMethod, tearDownMethod, fixtureInstance);
                        IsRunning = false;
                        return testItem;
                    }
                }
            }

            return null;
        }

        public static List<TestItem> RunTests(HashSet<string> ignoreList)
        {
            if (IsRunning)
            {
                throw new InvalidOperationException("Already running tests!");
            }

            var testItems = new List<TestItem>();

            IsRunning = true;

            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (type.IsDefined(typeof(TestFixtureAttribute), false))
                {
                    object fixtureInstance = Activator.CreateInstance(type);

                    MethodInfo[] methods = type.GetMethods();
                    MethodInfo setUpMethod = methods.FirstOrDefault(x => x.IsDefined(typeof(SetUpAttribute), false));
                    MethodInfo tearDownMethod = methods.FirstOrDefault(x => x.IsDefined(typeof(TearDownAttribute), false));

                    testItems.AddRange(methods.Select(testMethod => RunTest(testMethod, setUpMethod, tearDownMethod, fixtureInstance)).Where(testItem => testItem != null));
                }
            }

            IsRunning = false;

            return testItems;
        }

        private static TestItem RunTest(MethodInfo testMethod, MethodInfo setUpMethod, MethodInfo tearDownMethod, object fixtureInstance)
        {
            if (!testMethod.IsDefined(typeof(TestAttribute), false))
            {
                return null;
            }

            if (testMethod.DeclaringType == null)
            {
                throw new InvalidOperationException("Method must have a declaring type!");
            }

            var testItem = new TestItem
            {
                Name = testMethod.DeclaringType.Name + "::" + testMethod.Name,
                Success = true
            };

            try
            {
                // First run setup for the test if available
                if (setUpMethod != null)
                {
                    setUpMethod.Invoke(fixtureInstance, null);
                }
            }
            catch (Exception e)
            {
                testItem.Success = false;
                testItem.Message = e.ToString();
            }

            if (testItem.Success)
            {
                try
                {
                    // Run all cases if using the test case attribute
                    if (testMethod.IsDefined(typeof(TestCaseAttribute), false))
                    {
                        foreach (object testCase in testMethod.GetCustomAttributes(typeof(TestCaseAttribute), false))
                        {
                            var testCaseAttribute = testCase as TestCaseAttribute;
                            testMethod.Invoke(fixtureInstance, testCaseAttribute.Arguments);
                        }
                    }
                    else
                    {
                        testMethod.Invoke(fixtureInstance, null);
                    }
                }
                catch (Exception e)
                {
                    if (testMethod.IsDefined(typeof(ExpectedException), false))
                    {
                        var expectedEx = testMethod.GetCustomAttributes(typeof(ExpectedException), false)[0] as ExpectedException;
                        if (e.InnerException == null || expectedEx.ExceptionType != e.InnerException.GetType())
                        {
                            testItem.Success = false;
                            testItem.Message = e.ToString();
                        }
                    }
                    else
                    {
                        testItem.Success = false;
                        testItem.Message = e.InnerException.ToString();
                    }
                }
            }

            try
            {
                // Finally clean up regardless of previous results
                if (tearDownMethod != null)
                {
                    tearDownMethod.Invoke(fixtureInstance, null);
                }
            }
            catch (Exception e)
            {
                testItem.Success = false;
                testItem.Message = e.InnerException.ToString();
            }

            return testItem;
        }
    }
}