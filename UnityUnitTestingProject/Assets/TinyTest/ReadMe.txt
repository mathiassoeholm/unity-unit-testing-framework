Tiny [TEST] instructions:

1. Make sure you don't put the TinyTest folder in a folder calles plugins or Editor.
Otherwise the testing framework will be in another assembly as your game code.

2. Open the unit test window by going to Window -> Tiny Test -> Unit Test Window

3. Check out the example tests in the Example folder, to help you get started :-)